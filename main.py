import numpy as np
from matplotlib import pyplot as plt
from tensorflow.examples.tutorials.mnist import input_data
from Object import Object
from RBM import RBM

def load_data():
    data = input_data.read_data_sets("tmp/", one_hot=False)
    data = np.random.permutation(data.train.images)
    return data

def main():
    obj_rbm = Object()
    obj_rbm.data = load_data()
    obj_rbm.cd_step = 3
    obj_rbm.learning_rate = 0.001
    obj_rbm.batch_size = 64
    obj_rbm.epochs = 15
    rbm = RBM(obj_rbm)
    rbm.train()


if __name__ == '__main__':
   main()
