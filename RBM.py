import tensorflow as tf
import numpy as np
from matplotlib import pyplot as plt
###
### Essa RBM eh simples, ou seja nao apresenta todas as variaveis para melhorar o aprendizado que a complexa apresenta
###
class RBM():

    def __init__(self, params):

        self.graph = tf.Graph() # Grafo
        with self.graph.as_default(): # Abre o grafo
            num_v = 28 * 28 # numero de visiveis
            num_h = 512 # numero de ocultas

            # inicializa variaveis como nos do tensorflow
            self.W = tf.Variable(tf.truncated_normal((num_v,num_h))*0.01)
            self.bias_v = tf.Variable(tf.zeros((num_v,1)))
            self.bias_h = tf.Variable(tf.zeros((num_h,1)))

            # passos para o contrastivo
            self.cd = params.cd_step # quantas vezes Gibbs vai rodar
            self.final_model = None # guardar parametros finais aqui
            self.data = params.data # database
            self.learning_rate = params.learning_rate # taxa de aprendizado
            self.batch_size = params.batch_size # o tamanho do batch influencia no aprendizado
            self.epochs = params.epochs # loop para treinamento

    def gibbs_sample_h(self, v):
        # squeeze : Remove dimensoes do tamanho 1 da forma de um tensor.
        h = tf.sigmoid(tf.matmul(v, self.W) + tf.squeeze(self.bias_h))
        return tf.nn.relu(tf.sign(h - tf.random_uniform(h.shape)))

    def gibbs_sample_v(self, h):
        # squeeze : Remove dimensoes do tamanho 1 da forma de um tensor.
        v =tf.sigmoid(tf.matmul(h, tf.transpose(self.W)) + tf.squeeze(self.bias_v))
        return tf.nn.relu(tf.sign(v - tf.random_uniform(v.shape)))

    def gibbs_sample(self, i, j, v_step):
        h_step = self.gibbs_sample_h(v_step)
        v_step = self.gibbs_sample_v(h_step)
        return i + 1, j, v_step

    def calculed_energy(self, v):
        # o objetivo da energia eh sempre minimizar
        # primeiro reduzimos a soma do todo
        sum_h = tf.reduce_sum(tf.log(tf.exp(tf.matmul(v, self.W) + tf.squeeze(self.bias_h)) + 1), axis=1)
        # depois reduzimos a media
        return tf.reduce_mean(-sum_h - tf.matmul(v, self.bias_v))

    def calculed_loss(self,v,vk):
        return self.calculed_energy(v) - self.calculed_energy(vk)

    def calculed_optimizer(self,loss):
        return tf.train.AdamOptimizer(self.learning_rate).minimize(loss)

    def gibs_applied(self):
        with self.graph.as_default():
            v_tf = tf.placeholder(tf.float32, [self.batch_size, self.bias_v.shape[0]])
            # arredonda
            v = tf.round(v_tf) # inicializa v
            # retorne um tensor com a mesma forma e conteudo como entrada.
            v_copy = tf.identity(v) # inicializa vk

            # realiza k passos de amostragem de Gibbs
            i = tf.constant(0) # contador do loop
            num_cd = tf.constant(self.cd) # numero do cd

            i, num_cd, v_copy = tf.while_loop(
                                    cond = lambda i, step, *args: i <= step,
                                    body = self.gibbs_sample,
                                    loop_vars = [i,num_cd, v_copy],
                                    parallel_iterations=1,
                                    back_prop=False)

            v_copy = tf.stop_gradient(v_copy) # para nao fazer backprop pela amostragem Gibbs
            loss = self.calculed_loss(v,v_copy) # calculo do custo
            optimizer = self.calculed_optimizer(loss) # calcula a optimizacao
            init = tf.global_variables_initializer()
            return v_tf,loss,optimizer,init

    def train(self):
        v_tf,loss,optimizer,init =  self.gibs_applied()
        with tf.Session(graph=self.graph) as sess:
            init.run() # inicializa as variaveis
            step_batch = self.batch_size
            for j in range(self.epochs): # treina por epochs
                vlosses = []
                # pega cada parte do mini batch
                size_batch = len(self.data)- step_batch
                for i in range(0, size_batch , step_batch):
                    mini_batch = self.data[ i : i + self.batch_size] # faz o mini- batch tem aprendizado melhor
                    # retorna o loss calculado e o optimizador
                    rloss, roptmizer = sess.run([loss, optimizer], feed_dict={v_tf: mini_batch})
                    vlosses.append(rloss)
                # mostra o custo na epoca
                print('Custo de energia de cada epoca realizada  %d: ' % j, np.mean(vlosses))

            # salva os parametros do modelo
            self.final_model = self.W.eval()
            print(self.W.eval())
